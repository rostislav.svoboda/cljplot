(defproject org.clojars.bost/cljplot "0.0.3"
  :description "JVM chart library"
  :url "https://github.com/generateme/cljplot"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.11.1"]
                 [generateme/fastmath "2.1.8"]
                 [clojure2d "1.4.4"]
                 [clojure.java-time "0.3.3"]
                 [org.clojure/data.csv "1.0.1"]
                 [org.clojure/data.json "2.4.0"]]
  :repl-options {:timeout 120000}
  :java-source-paths ["src"]
  ;; :javac-options ["-target" "1.8" "-source" "1.8"]
  :target-path "target/%s"
  :deploy-repositories [["clojars" {:sign-releases false}]])
